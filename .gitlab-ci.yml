stages:
  - checks
  - tests
  - publish
  - coverage

variables:
  DOCKER_IMAGE: "registry.duniter.org/docker/python3/poetry"
  PYTHON_VERSION: "3.7"

image: $DOCKER_IMAGE/$PYTHON_VERSION:latest

.code_changes:
  only:
    changes:
      - bin/silkaj
      - silkaj/*.py
      - tests/*.py

.changes:
  extends: .code_changes
  only:
    changes:
      - .gitlab-ci.yml
      - pyproject.toml
      - poetry.lock

build:
  extends: .changes
  stage: checks
  script:
    - poetry build

format:
  extends: .code_changes
  stage: checks
  image: $DOCKER_IMAGE/3.8:latest
  script:
    - black --check bin silkaj tests

.tests:
  extends: .changes
  stage: tests
  image: $DOCKER_IMAGE/$PYTHON_VERSION:latest
  script:
    - poetry install
    - poetry run pytest

tests-3.5:
  extends: .tests
  tags: [poetry-56]
  variables:
    PYTHON_VERSION: "3.5"

tests-3.6:
  extends: .tests
  tags: [poetry-56]
  variables:
    PYTHON_VERSION: "3.6"

tests-3.7-coverage:
  extends: .tests
  tags: [poetry-78]
  script:
    - poetry install
    - poetry run pytest --cov silkaj --cov-report html:cov_html
    - poetry run coverage-badge -o cov_html/coverage.svg
  artifacts:
    paths:
      - cov_html
    expire_in: 2 days

tests-3.8:
  extends: .tests
  tags: [poetry-78]
  variables:
    PYTHON_VERSION: "3.8"

pypi_test:
  stage: publish
  only: [tags]
  when: manual
  script:
    - poetry config repositories.pypi_test https://test.pypi.org/legacy/
    - poetry publish --build --username $PYPI_TEST_LOGIN --password $PYPI_TEST_PASSWORD --repository pypi_test

pypi:
  stage: publish
  only: [tags]
  when: manual
  script:
    - poetry publish --build --username $PYPI_LOGIN --password $PYPI_PASSWORD

pages:
  extends: .changes
  needs: [tests-3.7-coverage]
  only: [dev]
  stage: coverage
  script: mv cov_html/ public/
  artifacts:
    paths:
      - public
    expire_in: 2 days
